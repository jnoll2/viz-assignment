
# Introduction

As before, you will submit this assignment via Git and #githost. This Visualization assignment tells you what to submit (#filename(visualization.R)), and how to do it (see "Instructions" below).

IMPORTANT: You MUST name your file #filename(visualization.R) exactly as shown, with NO SPACES, all lower case letters, and a capital #filename(.R) extension. Failure to do so will mean our marking script will not find your file, and you will receive a mark of zero (0) for this assignment.

*Pay attention!* If you do it wrong, you'll get a mark of zero (0), which will substantially reduce your chances of passing this module.

# Instructions

1.  Update #filename(research_question.yml)

    If you have changed either your research question (or your dataset, with our permission), since the Research Question assignment, you should update #filename(research_question.yml) to reflect these changes.

    If you have changed your dataset, you need to remove the old dataset, and add (and commit) your new dataset, as a CSV file.

    *Only* do this if you have received permission to change datasets!

2.  *Validate* your #filename(research_question.yml) file, using the #filename(validate_yaml.R) script from the Research Question lab repository. Be sure that the command executes successfully, and that *all* fields are populated correctly.

    We use the contents of #filename(research_question.yml) to interpret your visualization plots, so you must ensure it is correct and up-to-date. Invalid YAML files will result in a failing grade for the visualization deliverable.

3.  Create an R script called #filename(visualization.R)

    Using Notepad++ (or another text editor of your choice), or R Studio, create a file called #filename(visualization.R) in the Git workspace for your repository.

    IMPORTANT: *You MUST name your file #filename(visualization.R) exactly as shown, with NO SPACES, all lower case text, and a capital #filename(.R) extension. Failure to do so will mean our marking script will not find your file, and you will receive a mark of zero (0) for this assignment.*

    Your script should do the following:

    1.  Load any required libraries. Be sure these are part of *tidyverse* or base R; anything else will cause your script to fail, and you will receive zero (0) credit for this assignment.

    2.  Load your dataset from the *current working directory*. Do not, under any circumstances, hard code absolute paths into your script: this will guarantee that your script will fail execution, and you will receive a mark of zero (0) for this assignment.

    3.  Set the output to #filename(visualization.pdf):

             pdf("visualization.pdf")

        Note: you must name your output file #filename(visualization.pdf) so that our automatic marking scripts can find it. Failure to do so will result in a mark of zero (0) for this assignment.

    4.  Create a scatterplot, boxplot, or barplot as appropriate for visualizing your independent and dependent variables.

    5.  If your dependent variable is interval or ordinal kind, create a histogram with normal curve overlay.

4.  Commit #filename(visualization.R) to your Git repository workspace.

5.  Test #filename(visualization.R) to be sure it does what it's meant to do: from the *command line*, execute:

         Rscript visualization.R

6.  Use #command(git rm) to remove any extraneous files (editor backup files, old experiments, your old dataset if you changed datasets, and #filename(visualization.pdf) if you committed it mistakenly (never commit derived files: they get overwritten the very next time you run a script, and so they often create spurious merge conflicts).

7.  Push your clean, up-to-date, working workspace to BitBucket by 23:59 on #fmt_date(#cw_viz_due).

8.  Have one of your teammates clone your repository from a *different computer*, and test #filename(visualization.R) to be sure it does what it's meant to do,

# Assessment criteria

Your #filename(visualization.R) will be assessed on three major criteria:

1.  Does it work?

    -   Does the script load only libraries from *tidyverse* or base R?
    -   Does the script load the dataset from the current working #directory?
    -   Does the script write visualization.pdf into the current working directory?

2.  Is it correct?

    Does the script create the appropriate visualization for the research question and associated variables? If the dependent variable is interval or ordinal kind, does the output include a histogram with normal curve overlayed?

3.  Is it beautiful?

    -   Do the graphs have a main title?
    -   Are both the x and y axes labelled in human-friendly terms, including units?
    -   Do the 'tick' marks on the axes have appropriate labels, such as numbers with the right significant digits?
    -   Are the labels positioned so they don't overlap and are not cropped?
    -   Is color used judiciously to distinguish elements on the graphs?
    -   Is there a legend or key to explain colors?
    -   Are all titles, labels, tick marks, and other text *spelled correctly?*

# Want feedback in advance of the deadline?

Do you want us to comment on your visualization before you submit?

1.  Prepare a presentation using the #filename(visualization_presentation_template.pptx) template in this repository.

2.  Sign up on Canvas for a presentation slot (follow the *Calendar* link in the far left column). Be sure you have ticked "#module-#module_term-#module_year" under the "CALENDARS" list on the right side.

3.  Present your slides during your presentation slot.

**CAUTION**: groups who miss their presentation slot will be assessed a five (5) point penalty on their Visualization deliverable.

