## jhnoll@gmail.com
ROOT:=$(shell $(HOME)/bin/findup .sgrc)
SOURCE_DIR=.
BIB=references.bib
include $(ROOT)/tools/Makefile.in

# This is used by tools/Makefile.in to create 'all:' target.
INSTALL_OBJECTS=README.txt instructions.Rmd rubric.xlsx 

all: $(INSTALL_OBJECTS)

README.txt: ${TOOLS.dir}/templates/lab-README.in
	$(PP) -Dcomponent=${*} $(META_DATA) $< | $(PANDOC) --standalone -t plain -o $@

bREADME.md: ${TOOLS.dir}/templates/lab-README.in
	$(PP) $< > $@

rubric.xlsx: $(ROOT)/grades/visualization/rubric.xlsx
	cp $< $@


include $(ROOT)/tools/Make.rules
clean:
	rm README.md instructions.Rmd *.html *.pdf


